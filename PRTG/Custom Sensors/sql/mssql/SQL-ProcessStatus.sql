﻿-- Script to show the status of SQL Processes
-- dbo permissions are needed in order to see all processes
-- use master
-- DROP USER prtguser
-- CREATE LOGIN prtguser WITH PASSWORD = 'XXXXX'
-- CREATE USER prtguser FOR LOGIN prtguser
-- GRANT CONNECT TO prtguser
-- use DB-NAME
-- CREATE USER prtguser FOR LOGIN prtguser

SELECT (SELECT ISNULL(COUNT(1), 1) AS cnt FROM sys.sysprocesses WHERE status = 'background') AS background,
	   (SELECT ISNULL(COUNT(1), 1) AS cnt FROM sys.sysprocesses WHERE status = 'runnable')   AS runnable,
	   (SELECT ISNULL(COUNT(1), 1) AS cnt FROM sys.sysprocesses WHERE status = 'sleeping')   AS sleeping,
	   (SELECT ISNULL(COUNT(1), 1) AS cnt FROM sys.sysprocesses WHERE status = 'pending')    AS pending
