-- Script to show the status of SQL Processes
SELECT	(SELECT ISNULL(COUNT(1), 1) AS cnt FROM sys.sysprocesses WHERE status = 'background') AS background,
	(SELECT ISNULL(COUNT(1), 1) AS cnt FROM sys.sysprocesses WHERE status = 'runnable')   AS runnable,
	(SELECT ISNULL(COUNT(1), 1) AS cnt FROM sys.sysprocesses WHERE status = 'sleeping')   AS sleeping,
	(SELECT ISNULL(COUNT(1), 1) AS cnt FROM sys.sysprocesses WHERE status = 'pending')    AS pending
