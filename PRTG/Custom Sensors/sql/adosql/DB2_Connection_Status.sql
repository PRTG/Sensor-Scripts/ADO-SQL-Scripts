-- DB/2 Connection information
-- https://www.ibm.com/support/knowledgecenter/SSEPGG_10.1.0/com.ibm.db2.luw.sql.rtn.doc/doc/r0053938.html
-- ADO Conn String: Provider=IBMDADB2;Database=<DB>;Hostname=<IP or FQDN>;Protocol=TCPIP;Port=50000;Uid=<User ID>;Pwd=<Password>;
WITH MON_CONNS AS (SELECT * FROM TABLE(MON_GET_CONNECTION(cast(NULL as bigint), -2)) )
 SELECT (SELECT COUNT(*)  FROM MON_CONNS) AS Total_Connection
	   ,(SELECT SUM(DEADLOCKS)  FROM MON_CONNS) AS Deadlock_Cnt
	   ,(SELECT SUM(NUM_LOCKS_HELD)  FROM MON_CONNS) AS Locks_Held
	   ,(SELECT SUM(LOCK_WAITS)  FROM MON_CONNS) AS LockWait_Cnt
	   ,(SELECT SUM(LOCK_ESCALS)  FROM MON_CONNS) AS LockEscals_Cnt
	   ,(SELECT SUM(LOCK_TIMEOUTS)  FROM MON_CONNS) AS LockTimeout_Cnt
	   ,(SELECT SUM(THRESH_VIOLATIONS)  FROM MON_CONNS) AS Thresh_Viol_Cnt
	   ,(SELECT SUM(TOTAL_SORTS)  FROM MON_CONNS) AS Total_Sorts_Cnt
	   ,(SELECT SUM(SORT_OVERFLOWS)  FROM MON_CONNS) AS SortOverflow_Cnt
	   ,(SELECT SUM(ROWS_MODIFIED)  FROM MON_CONNS) AS RowsModified_Cnt
	   ,(SELECT SUM(ROWS_READ)  FROM MON_CONNS) AS RowsRead_Cnt
		 FROM SYSIBM.SYSDUMMY1
;